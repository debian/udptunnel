udptunnel (1.1-11) unstable; urgency=medium

  * debian/control: Add libnsl-dev to Build-Depends to fix (temporary) FTBFS.
    Closes: #1065161.

 -- Marcos Talau <talau@debian.org>  Sun, 10 Mar 2024 15:02:13 -0300

udptunnel (1.1-10) unstable; urgency=medium

  * debian/control: Bump Standards-Version to "4.6.2".
  * debian/copyright: Update packaging copyright years.
  * debian/patches/01-multicast.diff:
    - Add Bug-Debian field.
    - Change Forwarded to yes and add a description about that.
  * debian/patches/02-strlen-prototype.diff: Change Forwarded to yes and add a
    description about that.

 -- Marcos Talau <talau@debian.org>  Thu, 07 Sep 2023 11:06:33 +0530

udptunnel (1.1-9) unstable; urgency=medium

  * Update Maintainer mail.
  * debian/source/lintian-overrides:
    - Update maintainer-manual-page.
    - Update very-long-line-length-in-source-file.

 -- Marcos Talau <talau@debian.org>  Fri, 09 Sep 2022 13:43:56 -0300

udptunnel (1.1-8) unstable; urgency=medium

  * debian/control: Bump Standards-Version to 4.6.1, no changes.
  * debian/copyright: Update packaging copyright years.
  * debian/source/lintian-overrides: Add an override about long line in
    the manpage.
  * debian/tests/validation: New. Create a validation test.

 -- Marcos Talau <marcos@talau.info>  Fri, 10 Jun 2022 16:13:28 -0300

udptunnel (1.1-7) unstable; urgency=medium

  * Upload to unstable.
  * debian/control:
    - Bump Standards-Version to 4.6.0.1.
    - Update Vcs-* fields.

 -- Marcos Talau <marcos@talau.info>  Fri, 10 Sep 2021 20:03:58 -0300

udptunnel (1.1-6) experimental; urgency=medium

  * Using new DH level format. Consequently:
    - debian/compat: Remove.
    - debian/control: Change from 'debhelper' to 'debhelper-compat' in
      Build-Depends field and bump level to 13.
  * debian/control:
    - Add Rules-Requires-Root field.
    - Bump Standards-Version to 4.5.1.
    - Change Maintainer mail.
    - Update Vcs fields to salsa.
  * debian/copyright: Update years.
  * debian/patches/:
    - 01-multicast.diff: Add meta-info Forwarded and Author name.
    - 02-strlen-prototype.diff: Add meta-info Forwarded.
  * debian/rules: Remove '--with autoreconf' because it is default since DH 10.
  * debian/salsa-ci.yml: Add to provide CI tests for Salsa.
  * debian/source/lintian-overrides: New file, add an override.
  * debian/tests/*: Create autopkgtest.
  * debian/udptunnel.1: Fix typo.
  * debian/upstream/metadata: Add upstream metadata information.
  * debian/watch: Replace some regex's to @TAGS@.

 -- Marcos Talau <marcos@talau.info>  Fri, 02 Jul 2021 22:09:59 -0300

udptunnel (1.1-5) unstable; urgency=medium

  * DH level updated to 9.
  * Switch from autoconf to autoreconf.
  * debian/control
    - More info in Description.
    - Standards-Version updated to 3.9.7
    - Vcs-* fields to canonical URI.
  * debian/copyright
    - Added Jonathan Lennox to Copyright clause.
    - Added previous Debian Maintainers to Copyright clause.
    - Updated to 1.0 Machine-readable format.
  * debian/dirs
    - Removed, as it is dispensable.
  * debian/rules
    - Added Hardening options.
  * debian/watch
    - Improved URL regexp.
    - Updated to version 4.

 -- Marcos Talau <talau@users.sourceforge.net>  Mon, 04 Apr 2016 19:44:41 -0300

udptunnel (1.1-4) unstable; urgency=low

  * New maintainer (Closes: #590779)
    - Thanks to Chris Lamb
  * debian/control
    - New Vcs-* address
    - Standards-Version updated to 3.9.2
      - No changes need
  * config.* is now updating with debhelper
  * Switch to dpkg-source 3.0 (quilt) format
  * DEP-3 in patches
  * DEP-5 in debian/copyright
  * debian/compat
    - Compatibility level to 8

 -- Marcos Talau <talau@users.sourceforge.net>  Fri, 21 Oct 2011 10:27:03 -0200

udptunnel (1.1-3) unstable; urgency=low

  * Update maintainer email address.
  * Update Git repository locations.
  * Use `dh_prep` over deprecated `dh_clean -k`.
  * Add patch description for 02-strlen-prototype.diff.

 -- Chris Lamb <lamby@debian.org>  Mon, 16 Feb 2009 02:50:52 +0000

udptunnel (1.1-2) unstable; urgency=low

  * New maintainer (Closes: #472603)
  * Acknowledge NMU (Closes: #254834)
  * Rework packaging:
    - Move to debhelper 7.
    - Extract patches and adopt quilt patch system.
    - Move manpage to under debian/.
    - Add Build-Depends on autotools-dev.
    - Move to machine-parsable debian/copyright.
    - Add debian/watch file.
  * Add 02-strlen-prototype.diff to avoid implicit definition of strlen().

 -- Chris Lamb <chris@chris-lamb.co.uk>  Thu, 28 Aug 2008 07:42:58 +0100

udptunnel (1.1-1.1) unstable; urgency=low

  * NMU: Change htons()->htonl() to fix multicast (Closes: #254834).

 -- Paul Sladen <debian@paul.sladen.org>  Sun, 21 May 2006 15:00:02 +0100

udptunnel (1.1-1) unstable; urgency=low

  * Initial Release.

 -- Thomas Scheffczyk <thomas.scheffczyk@verwaltung.uni-mainz.de>  Wed,  2 Apr 2003 15:55:23 +0200
