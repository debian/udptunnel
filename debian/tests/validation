#!/bin/sh

UDPSERVER_PORT=5500
UDPTUNNEL_SRV_TCP_PORT=6491
UDPTUNNEL_CLI_UDP_PORT=6661

TX_FILE="$AUTOPKGTEST_TMP/tx.data"
RX_FILE="$AUTOPKGTEST_TMP/rx.data"

STATUS_ERROR=1
STATUS_SKIP=77

test_udpserver_port_open() {
    netstat --udp -ln | grep $UDPSERVER_PORT

    if [ "$?" != 0 ]; then
        echo "UDP server is not listening at port UDP/$UDPSERVER_PORT"
	echo "ending $0 with status $STATUS_SKIP"
	exit $STATUS_SKIP
    fi
}

test_udptunnel_tcp_port_open() {
    netstat --tcp -ln | grep $UDPTUNNEL_SRV_TCP_PORT

    if [ "$?" != 0 ]; then
        echo "UDPTunnel server is not listening at port TCP/$UDPTUNNEL_SRV_TCP_PORT"
	echo "ending $0 with status $STATUS_ERROR"
	exit $STATUS_ERROR
    fi
}

test_udptunnel_cli_port_open() {
    netstat --udp -ln | grep $UDPTUNNEL_CLI_UDP_PORT

    if [ "$?" != 0 ]; then
        echo "UDPTunnel client is not listening at port UDP/$UDPTUNNEL_CLI_UDP_PORT"
	echo "ending $0 with status $STATUS_ERROR"
	exit $STATUS_ERROR
    fi
}

start_udp_server() {
    ncat -u -l 127.0.0.1 $UDPSERVER_PORT -o $RX_FILE &
    sleep 3

    test_udpserver_port_open
}

start_udptunnel_server() {
    udptunnel -s $UDPTUNNEL_SRV_TCP_PORT 127.0.0.1/$UDPSERVER_PORT &
    sleep 3

    test_udptunnel_tcp_port_open
}

start_udptunnel_client() {
    udptunnel -c 127.0.0.1/$UDPTUNNEL_SRV_TCP_PORT 127.0.0.1/$UDPTUNNEL_CLI_UDP_PORT &
    sleep 3

    test_udptunnel_cli_port_open
}

make_tx_file() {
# trailing whitespaces were left on purpose
cat <<EOF >> $TX_FILE
THIS  
 IS  
  THE  
   DATA 
    SEND 
     OVER 
      UDPTunnel 
EOF
}

transmit_udp_data() {
    make_tx_file
    cat $TX_FILE | ncat -u 127.0.0.1 $UDPTUNNEL_CLI_UDP_PORT
}

received_udp_data() {
    diff $TX_FILE $RX_FILE

    return $?
}

test_communication() {
    transmit_udp_data

    if ! received_udp_data; then
        echo "UDPTunnel communication failed"
	echo "ending $0 with status $STATUS_ERROR"
        exit $STATUS_ERROR
    fi
}

## main
start_udp_server
start_udptunnel_server
start_udptunnel_client

test_communication

exit 0
